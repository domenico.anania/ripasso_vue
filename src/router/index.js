import { createRouter, createWebHistory } from 'vue-router'
const routes = [
  {
    path: '/permalink/:id',
    name: 'permalink',
    component: () => import('../views/PermalinkView.vue'),
    props: true,
  },
  {
    path: '/:id?',
    name: 'ProductsView',
    component: () => import('../views/ProductsView.vue'),
    props: true,
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
